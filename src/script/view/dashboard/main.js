import "../../component/dashboard/bg-menu-search.js";
import "../../component/dashboard/menu-list.js";
import "../../component/dashboard/card-list.js";
import "../../component/footer-bar";
import  DataSource  from '../../data/data-source';


const main = function() {
    const searchElement = document.querySelector("bg-menu-search");
    const movieListElement = document.querySelector("card-list");


    const loadMovie = async() =>{
        try {
            const result = await DataSource.searchMovie();
            renderResult(result);
        } catch (error) {
           fallbackResult(error);
            
        }
    }
    
    loadMovie();

    const onButtonSearchClicked = async() => {
        try {
            const result = await DataSource.searchMovie(null, searchElement.value);            
            renderResult(result);
        } catch (error) {
           fallbackResult(error);
            
        }
    }

    const renderResult = (result) => {
        
        movieListElement.movies = result;
    }

    const fallbackResult = (message) => {
        movieListElement.renderError(message);
    }

    searchElement.clickEvent = onButtonSearchClicked;

};

export default main;