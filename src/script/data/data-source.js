class DataSource{
    static searchMovie(endpoint, keyword) {
        let url_api = "https://api.themoviedb.org/3/";
        let api_key = "7117559b48877bf3558dc38bb99c36e3";
        if(endpoint == null){
            endpoint = 'now_playing';
        }


        let uri_default = `${url_api}search/movie?api_key=${api_key}&language=en-US&page=1&query=${keyword}`;
        if(keyword == null){
             uri_default = `${url_api}movie/${endpoint}?api_key=${api_key}&language=en-US&page=1`;
        }

         
        return fetch(uri_default)
        .then(response => {
            return response.json();
        })
        .then(responseJson => {
            if(responseJson.results){
                return Promise.resolve(responseJson.results);

            }else{
                return Promise.reject(`${keyword} Not Found`);
            };

        })
    };
}

export default DataSource;
