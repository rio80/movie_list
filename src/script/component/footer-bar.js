class FooterBar extends HTMLElement{
    constructor(){
        super();
        this.shadowDOM = this.attachShadow({mode : 'closed'});
    }

    connectedCallback(){
        this.render();
    }

    render(){
       this.shadowDOM.innerHTML =  `
       <style>
                       
           #footer {
               background-color: azure;
               margin: 0px auto;
               text-align: center;
               padding: 10px 20px;
           }
       </style>

       <div id="footer">
            copyright : Rio Firmansyah Eka Saputra
       </div>
       `
    }
}

customElements.define('footer-bar', FooterBar);