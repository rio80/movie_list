import "../../component/dashboard/card-list.js";
import  DataSource  from '../../data/data-source';

class MenuList extends HTMLElement {
    constructor(){
        super();
        this.shadowDOM = this.attachShadow({mode: 'open'});
    }

    connectedCallback(){
        this.render();
    }

    set clickEvent(event) {
        this._clickEvent = event;
        this.render();
    }

    listMenu() {
        return [
            {
                'text' :'Now Playing',
                'endpoint': 'now_playing'
            },
            {
                'text' : 'Popular',
                'endpoint': 'popular'
            },
            {
                'text'  :'Upcoming',
                'endpoint': 'upcoming'
            },
            {
                'text'  :'Top Rated',
                'endpoint': 'top_rated'
            }
        ]
    } 

    render(){
        
        let setHtml = `
        <style>
            #menu {
                /* display: flex; */
                justify-content: center;
                text-align: center;
                overflow: auto;
                padding-top: 10px;
                padding-bottom: 10px;
                position: sticky;
                top: 0;
                background-color: #503284;
            }
            
            #menu > .endpoint{
                width: 25%;
                padding: 4px;
                border-radius: 5px;
                background-color: #ffff00;
                color: #000099;
                text-decoration: none;
                font-size: 14px;
                float: left;
                font-weight: bold;
            }
            
            #menu > .endpoint:hover {
                background-color: #0000cc;
                color: #ffffff;
                text-decoration: underline;
            }
            
            #menu > .endpoint:active {
                color: #000000;
                background-color: #ffffff;
                font-weight: 100;
            }
            
            @media screen and (max-width: 500px) {
                #menu {
                    padding-right: 10%;
                    position: static;
            
                }
                #menu > .endpoint {
                    width: 100%;
            
                }
            }
        </style>
        <div id="menu">
            `;
            
            this.listMenu().map(menu => {
                setHtml += `<button class="endpoint" id="${menu.endpoint}">${menu.text}</button>`;
            })

            setHtml += '</div>';

            this.shadowDOM.innerHTML = setHtml;
            
           
           
            this.listMenu().map(menu => {
                this.shadowDOM.querySelector(`#${menu.endpoint}`).addEventListener("click", menuClick);
                const movieListElement = document.querySelector("card-list");
                async function menuClick(){
                    try {
                        const result = await DataSource.searchMovie(menu.endpoint); 
                        movieListElement.movies = result;
                    } catch (error) {
                        movieListElement.renderError(error);
                    }
                }
            })          
    }
}

customElements.define('menu-list', MenuList);