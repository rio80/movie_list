class CardItem extends HTMLElement {
    constructor(){
        super();
        this.shadowDOM = this.attachShadow({mode : 'open'});

    }

     connectedCallback(){
        this.render();
    }


    set movie(movie){
        this._movie = movie;
        this.render();
    }

   set urlImage(url){
       this._url = url;
   }
    

    render(){
        let url_image_backdrop;
        if(this._movie.backdrop_path != null){
             url_image_backdrop = `https://image.tmdb.org/t/p/w500/${this._movie.backdrop_path}`;
        }
        
        let setHtml = `
            <style>
            :host {
                background: white;
                margin: 5px 5px 2em 5px;
                border-radius: 6px;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                transition: 0.3s;
                width:800px;
            }
            
            
            .card img {
                width: 100%;
                max-height: 300px;
                object-fit: cover;
                object-position: center;
            }
            
            .card > a {
                color: black;
                text-decoration: none;
            }
            
            .card > a:hover {
                box-shadow: 3px 3px 8px hsl(0, 0%, 80%);
            }
            
            .card-content {
                padding-right: 10px;
                padding: 10px;
                padding-top:0px;
                padding-bottom: 0.2em;
            }
            
            .card-content > p {
                overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                -webkit-box-orient: vertical;
                -webkit-line-clamp: 4;
            }
            
            .card-content > h2 {
                font-weight: 100;
            }
            
            .card-content p {
                font-size: 80%;
            }
            
            #read-more{
                color: darkslateblue;
                margin-right: 10px;
                margin-bottom: 10px;
            }
            
            @media screen and (min-width: 40em) {
                
                .cards {
                    flex-wrap: wrap;
                    justify-content: space-evenly;
                    margin-bottom: 6px;
                    overflow: hidden;
                }
            
                .card {
                    width:1000px;
                    background: white;
                    margin:0 auto;
                    border-radius: 6px;
                    
                }
            }
            
            @media screen and (min-width: 60em) {
            
                .card {
                    background: white;
                border-radius: 6px;

                }
            }
            </style>

            <section class="cards">
           
            <article class="card">
                <a href="#">
                    <picture class="thumbnail">
                        <img src="${url_image_backdrop}" alt="${this._movie.title}">
                    </picture>
                    <div class="card-content">
                
                         <h2>${this._movie.original_title}</h2>
                        <p>${this._movie.overview}</p>
                    </div><!-- .card-content -->
                </a>
                <!--<div id="read-more">
                    <a href="http://localhost:8087/src/detail.html">read more</a>
                </div>-->
            </article><!-- .card -->
           
        </section>
        `;

        this.shadowDOM.innerHTML = setHtml;
    }
}

customElements.define('card-item', CardItem);