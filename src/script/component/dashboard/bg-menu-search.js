class BgMenuSearch extends HTMLElement {
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode : 'open'});
    }

    connectedCallback(){
        this.render();
    }

    set clickEvent(event) {
        this._clickEvent = event;
        this.render();
    }

    render() {
        let setHtml = `
            <style>
            #bg-menu{
                background-color: #503284;
                overflow: auto;
            }
            
            #top-bar{
                padding:10px 0 0 20px;
                position: relative;
            }
            
            #title {
                color: #ffffff;
                float: left;  
                font-size: 2em;
            }
            
            #search {
                float: right;
                margin-top: 10px;
                margin-bottom: 20px;
                display:inline-block;
                width:500px;
            }
            
            #search > input, button {
                font-size: 1em;
                padding: 10px;
                display: inline-block;
                outline: 0 none;
            }
            
            #search > input[type="text"]{
                border: 0px;
                border-radius: 4px 0 0 4px;
                width: 70%;
                margin:0;
                padding-right: 100px;
            }
            
            #search > button {
                width: 20%;
                margin: 0px;
                box-sizing:border-box;
                height: 39px;
                border: 0;
                margin-left: -20%;
                cursor:pointer;
                background-color: aliceblue;
            }
            
            #search > button:focus {
                background-color: hsl(0, 0%, 80%)
            }

            @media screen and (max-width: 500px) {
                #search {
                    float: none;
                    margin-top: 10px;
                    margin-bottom: 20px;
                    display:inline-block;
                    width: 100%;
                }

                #search > input[type="text"]{
                    border: 0px;
                    border-radius: 4px 0 0 4px;
                    width: 70%;
                    padding-right: 60px;
                }

                #search > button {
                    width: 20%;
                    margin: 0px;
                    box-sizing:border-box;
                    height: 39px;
                    border: 0;
                    margin-left: -20%;
                    cursor:pointer;
                    background-color: aliceblue;
                }
                
               
              }
            </style>

            <div id="bg-menu">
                <div id="top-bar">
                    <div id="title">Daftar Film</div>
                        <div id="search">
                            <input type="text" name="text-search" id="text-search" placeholder="Search Movie">
                            <button id="button-search">Cari</button>
                        </div>
                </div>
            </div>
        `;
        
        this.shadowDOM.innerHTML = setHtml;

        this.shadowDOM.querySelector('#button-search').addEventListener("click", this._clickEvent);

    }

    get value(){
        return this.shadowDOM.querySelector('#text-search').value;
    }
}

customElements.define('bg-menu-search', BgMenuSearch);