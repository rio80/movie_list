import  "./card-item.js";

class CardList extends HTMLElement {
    constructor(){
        super();
        this.shadowDOM = this.attachShadow({mode : 'open'});
        
    }
 
    set movies(movies){
        this._movies = movies;
        this.render();
    }

    render(){
        this.shadowDOM.innerHTML = "";
        
        this._movies.map(movie => {
            const movieItemElement = document.createElement("card-item");
            movieItemElement.movie = movie;
            this.shadowDOM.appendChild(movieItemElement);
        });
    }

    renderError(message){
        this.shadowDOM.innerHTML = `
        <style>
                
        .placeholder {
            font-weight: lighter;
            color: rgba(0,0,0,0.5);
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            margin: 0px auto;
        }
        </style>`;

        this.shadowDOM.innerHTML += `<h2 class="placeholder">${message}</h2>`;
    }
}

customElements.define('card-list', CardList);