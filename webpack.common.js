const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");

module.exports = {
    entry: {
        app: "./src/app.js",
    },
    output: {
        path: path.resolve(__dirname, "public"),
        filename: "[name].bundle.js"
    },

    module: {
        rules: [
            // {
            //     test: /\.(jpg|png)$/,
            //     use: [{
            //             loader: "file-loader",
            //             options: {
            //                 name: '[name].[ext]',
            //                 outputPath: 'img/',
            //                 publicPath: 'img/',
            //             }
            //         }
            //     ]
            // },
            {
                test: /\.css$/,
                use: [{
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    }
                ]
            },
            // {
            //     test: /\.html$/,
            //     use: [{
            //         loader: "file-loader",
            //         options: {
            //             name: '[name].[ext]'
            //         }
            //     }],
            //     exclude: path.resolve(__dirname, 'src/index.html'),
            // }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: "index.html",
            chunks: ['app'],
        }),
        new HtmlWebpackPlugin({
            template: "./src/detail.html",
            filename: "detail.html",
            // chunks: ['app'],
        }),
    ],
    devServer: {
        host: "localhost", // Your Comput er Name
        port: 8087
    }
}